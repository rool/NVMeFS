/*
 * Copyright (c) 2005, Thomas Milius Stade
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Stader Softwareentwicklung GmbH nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/* Module Memory Management */

#ifndef mod_mem_manag_h
#define mod_mem_manag_h

#include <stdbool.h>
#include <stddef.h>

/* !!!!!!!!!! definitions !!!!!!!!!! */
/* For other libraries to use this part of code */
#ifndef ALIGNED_ALLOC_DEF
#define ALIGNED_ALLOC_DEF(x, y) mod_mem_manag_aligned_alloc_s(x, y)
#endif

#ifndef MALLOC_DEF
#define MALLOC_DEF(x) mod_mem_manag_malloc_s(x)
#endif

#ifndef FREE_DEF
#define FREE_DEF(x) mod_mem_manag_free_s(x)
#endif

#ifndef REALLOC_DEF
#define REALLOC_DEF(x, y) mod_mem_manag_realloc_s(x, y)
#endif

/* !!!!!!!!!! functions !!!!!!!!!! */
/** Frees a piece of memory inside the given
    dynamic area. */
extern void mod_mem_manag_free(int dynamic_area_no,
                               void *ptr);

/** Frees a piece of memory inside an internally
    stored dynamic area. */
extern void mod_mem_manag_free_s(void *ptr);

/** Claims an aligned piece of memory of requested size inside the given
    dynamic area. */
extern void *mod_mem_manag_aligned_alloc(int dynamic_area_no,
                                         size_t alignment,
                                         size_t size);

/** Claims an aligned piece of memory of requested size inside an internally
    stored dynamic area. */
extern void *mod_mem_manag_aligned_alloc_s(size_t alignment,
                                           size_t size);

/** Claims a piece of memory of requested size inside the given
    dynamic area. */
extern void *mod_mem_manag_malloc(int dynamic_area_no,
                                  size_t size);

/** Claims a piece of memory of requested size inside an internally
    stored dynamic area. */
extern void *mod_mem_manag_malloc_s(size_t size);

/** Resizes a piece of memory to requested size inside the given
    dynamic area. note that the location may change during this
    process even data will be transfered to new location. */
extern void *mod_mem_manag_realloc(int dynamic_area_no,
                                   void *ptr,
                                   size_t size);

/** Resizes a piece of memory to requested size insidee an internally
    stored dynamic area. note that the location may change during this
    process even data will be transfered to new location. */
extern void *mod_mem_manag_realloc_s(void *ptr,
                                     size_t size);

/** Generates the given dynamic area incl. its memory management.

    You must give the maximal size of the area and its expansion
    size. */
extern int mod_mem_manag_dynamic_area_initialize(char *area_name,
                                                 unsigned long max_area_size,
                                                 unsigned long expansion_size);

/** Releases the given dynamic area incl. its memory management. */
extern bool mod_mem_manag_dynamic_area_release(int dynamic_area_no);

#endif
